import React from "react"
import ReactDOM from "react-dom"

/*
console.dir(document.createElement("div")) // DOM
console.dir(<div></div>) // ReactDOM
*/

function App () {
    return(
        <h1>Курс Валют</h1>
    )
}

ReactDOM.render(<App/>, document.querySelector("[color]"))
